import 'package:dinamyc_theme_color/blocs/theme.dart';
import 'package:dinamyc_theme_color/pages/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => ThemeChanger(ThemeData.dark()),
      child: MaterialAppTheme(),
    );
  }
}

class MaterialAppTheme extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final theme=Provider.of<ThemeChanger>(context);


    return MaterialApp(
      home: HomePage(),
      theme: theme.getTheme(),
    );
  }
}
