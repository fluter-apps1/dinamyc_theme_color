import 'package:dinamyc_theme_color/blocs/theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final tema = Provider.of<ThemeChanger>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('appbar'),
      ),
      body: ListaThemas(),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () => {
                tema.setTheme(ThemeData(
                    brightness: Brightness.light, primaryColor: Colors.amber))
              }),
    );
  }
}

class ListaThemas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final tema = Provider.of<ThemeChanger>(context);

    return Container(
      child: Column(
        children: <Widget>[
          FlatButton(
              onPressed: () => tema.setTheme(ThemeData.light()),
              child: Text('light')),
          FlatButton(
              onPressed: () => tema.setTheme(ThemeData.dark()),
              child: Text('dark'))
        ],
      ),
    );
  }
}
